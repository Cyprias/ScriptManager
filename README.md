# ScriptManager

## Synopsis

[SkunkWorks](http://skunkworks.sourceforge.net) script to run multiple scripts simultaneously.

## Functionality
- Search sibling folders for SkunkScript ([SkunkSuite](https://gitlab.com/Cyprias/SkunkSuite)) based scripts.
- List scripts in a GUI window on the Decal bar.
- Enable and disable scripts from in game window.

## Installation
- Install Decal. [decaldev.com](https://www.decaldev.com)
- Install SkunkWorks for Asheron's Call. [SkunkWorks35-500.exe](https://sourceforge.net/projects/skunkworks/files/SkunkWorks/3.5/SkunkWorks35-500.exe/download)
- Download updated SkunkWorks skapi.dll file. [SkunkWorks3.5.509.zip](https://sourceforge.net/projects/skunkworks/files/SkunkWorks/3.5/SkunkWorks3.5.509.zip/download)
- Extract SkunkWorks3.5.509.zip to your SkunkWorks directory, overwriting the existing skapi.dll file.
- Download [ScriptManager](https://gitlab.com/Cyprias/ScriptManager/tags) and extract it to your SkunkWorks directory. 
- Log into Asheron's Call, click the SkunkWorks icon on your Decal bar and select ScriptManager.swx from the dropdown menu. You may need to use the browser [...] button to locate it.


## License
MIT License	(http://opensource.org/licenses/MIT)