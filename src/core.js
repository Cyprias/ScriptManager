/********************************************************************************************\
	Script:         ScriptManager-1.0
	Purpose:        Run multiple SkunkWorks scripts simultaneously.
	Author:         Cyprias
	Date:           2018/08/30
	Licence:        MIT License	(http://opensource.org/licenses/MIT)
\*********************************************************************************************/

/*\
	ScriptManager scans sibling folders for scripts to load. Such scripts must use the SkunkScript framework and have a package.json file in their root directory.
	The package.json file should have a name, description, main and icon fields. Example
	{	
		"name": "LootProfile-3.3",
		"description": "Loot Profiler",
		"main": "LootProfile3.js",
		"icon": 7919
	}
	
	ScriptManager can either be the main script running or loaded from another script. Example of how to load ScriptManager from another script.
	function loadScriptManager() {
		// Check if ScriptManager is already loaded.
		if (typeof LibStub("SkunkScript-1.0").getScript("ScriptManager", true) !== "undefined") {
			debug("ScriptManager is already running.");
			return;
		}
		
		// Check if ScriptManager is in a sibling folder next to the running script.
		var path = "..\\ScriptManager\\src\\core.js";
		if (fs.existsSync(path)) {
			debug("Loading ScriptManager...");
			var script = require(path);
			script.initialize();
			script.enable();
		}
	};
	
\*/

var MAJOR = "ScriptManager-1.0";
var MINOR = 181012; // Year Month Day

(function (factory) {
	// Load our dependencies.
	// LibStub libraries.
	require("SkunkSuite\\LibStub");
	require("SkunkSuite\\SkunkScript-1.0");
	require("SkunkSuite\\SkunkEvent-1.0");
	require("SkunkSuite\\SkunkSchema-1.0");
	require("SkunkSuite\\SkunkFS-1.0");
	require("SkunkSuite\\SkunkLogger-1.0");

	require("SkunkSuite\\json2.js");
	
	var dependencies = {};
	dependencies.EventEmitter   = (typeof EventEmitter !== "undefined" && EventEmitter) || require("modules\\SkunkSuite\\EventEmitter"); 

	// Check if script was loaded via require().
    if (typeof module === 'object' && module.exports) {
		// Exports. 
        module.exports = factory(dependencies);
	} else {
		// Script was loaded via swx file, set StanceApp as a global object.
        ScriptManager10 = factory(dependencies);
	}
}(function ( dependencies ) {
	var EventEmitter = dependencies.EventEmitter;
	var core = LibStub("SkunkScript-1.0").newScript(new EventEmitter(), MAJOR, "SkunkEvent-1.0", "SkunkSchema-1.0", "SkunkFS-1.0", "SkunkLogger-1.0");
	core.version = MAJOR + "." + MINOR; 
	core.title = core.version;
	
	var debugging = false;

	var SkunkSchema;
	
	core.debug = function debug() {
		var args = Array.prototype.slice.call(arguments);
		if (debugging != true) return;

		skapi.OutputLine("[SM|D] " + args.join('\t'), opmConsole);
	};

	core.inWaitEvent = false;
	core.WaitEvent = function WaitEvent(duration, wem, evid) {
		core.inWaitEvent = true;
		var result = skapi.WaitEvent(duration, wem, evid);
		core.inWaitEvent = false;
		return result;
	};
	
	core.getPackages = function getPackages() {
		var packages = [];
		
		var fso = new ActiveXObject("Scripting.FileSystemObject");
		var f = fso.GetFolder("..\\");
		var fc = new Enumerator(f.SubFolders);
		var files;
		var subFolder;
		var fileName;
		var path;
		var package;
		for   (;   !fc.atEnd();   fc.moveNext()) {
			subFolder = fc.item();
			//consoleLog(subFolder);
			files = new Enumerator(subFolder.files);
			for (;   !files.atEnd();   files.moveNext()) {
				if (files.item().Name == "package.json") {
					path = files.item().Path;

					package = core.getJsonFile({path:path});
					packages.push({path:path, name:package.name, description:package.description, main:package.main, icon:package.icon});
				}
			}
		}
		return packages;
	};
	
	core.getScriptMainFile = function getScriptMainFile(path) {
		core.debug("<getScriptMainFile>", path);
		var fso = new ActiveXObject("Scripting.FileSystemObject");
		if (!fso.FileExists(path)) return;
		var file = fso.GetFile(path);
		var folder = file.ParentFolder;
		
		var package = core.getJsonFile({path:path});
		
		//consoleLog("package: " + package);
		if (!package) return;
		
		var mainFilePath = folder.Path + "\\" + package.main;
		core.debug(" mainFilePath: " + mainFilePath);
		return fso.GetFile(mainFilePath);
	};
	
	var scriptModules = [];
	core.loadScript = function loadScript( params ) {
		var path = params.path;
		core.debug("<loadScript>", path);
		
		var fso = new ActiveXObject("Scripting.FileSystemObject");
		var file = fso.GetFile(path);
		var dirname = file.ParentFolder.Path;
		
		var mainFile = getScriptMainFile(path);
		core.debug(" requiring " + mainFile.Path + " (" + dirname + ")...");

		var script = require(mainFile.Path, {dirnameOverride:dirname}); // __dirname
		//var script = require(mainFile.Path, undefined);
		

		loadedScripts[path] = script;
		
		if (typeof script.on === "function") {
			script.on("onEnable", core.populateScriptList);
		} else {
			var oldOnEnable = script.onEnable;
			script.onEnable = function onEnable() {
				oldOnEnable.apply(this, arguments);
				core.populateScriptList();
			};
		}
		
		if (typeof script.on === "function") {
			script.on("onDisable", core.populateScriptList);
		} else {
			var oldOnDisable = script.onDisable;
			script.onDisable = function onDisable() {
				oldOnEnable.apply(this, arguments);
				core.populateScriptList();
			};
		}
		
		return script;
	};
	
	var loadedScripts = {};
	core.getScript = function getScript(params) {
		core.debug("<getScript>", "name: " + params.name, "path: " + params.path);
		var path = params.path;
		if (params.name) {
			var package = core.getPackage({name:params.name});
			core.debug("package: " + (typeof package));
			path = package && package.path || path;
		}
		if (path) {
			return loadedScripts[path] || loadScript({path:path});
		}
	};

	function enableScript(params) {
		var path = params.path;
		var active = params.active;
		core.debug("<enableScript>", path);

		//var script = loadedScripts[path] || loadScript({path:path});
		var script = getScript({path:path});
		if (!script) {
			core.warn("Failed to get script object!");
			return;
		}

		if (!script.isInitialized()) {
			script.initialize();
		}
		script.enable({active:active});
	};
	
	function disableScript(params) {
		var path = params.path;
		core.debug("<disableScript>", path);
		
		var script = loadedScripts[path];
		if (script) {
			script.disable();
		}
	};
	
	var onControlEvent = function factory() {
		return function onControlEvent(control, szPanel, szControl, value) {
			if (szControl == "lstScripts") {
				var intCol = parseInt(value.split(",")[0]);
				var intRow = parseInt(value.split(",")[1]);
				//debug("intCol: " + intCol + ", intRow: " + intRow);
				var package = packages[intRow];
				
				if (intCol == 0) {
					console.Clear( );

					control.getListPropertyAsync(0, intRow, function gotListProperty(err, value) {
						if (err) return core.error("Error getting list's checkbox value", err);
						if (value == true) {
							enableScript({path:package.path, active:true});
						} else {
							disableScript({path:package.path});
						}
					});
					
				} else if (intCol > 0) {
					var name = package.name;
					var description = package.description;
					core.info(name + ": " + description);
				}
			} else if (szControl == "btnQuit") {
				core.disable();
			}
		};
	}();

	core.controls = {};
	core.createGUI = function createGUI() {

		var view = core.controls.view = core.createView({
			title:  core.title, 
			width:  200, 
			height: 200, 
			icon:   9507
		});
	
		var layout = new SkunkSchema.FixedLayout({parent:view});

		var notebook = new SkunkSchema.Notebook({
			parent:layout, 
			name:"notebook"
		})
		.setWidth(view.getWidth())
		.setHeight(view.getHeight()-20); // leave 20px for footer buttons
		
		var page = new SkunkSchema.Page({
			parent:notebook, 
			label:"Scripts"
		});
		
		var lstScripts = core.controls.lstScripts = new SkunkSchema.List({
			parent:page, 
			name:"lstScripts"
		})
		.setWidth(page.getWidth())
		.setHeight(page.getHeight()-20)
		.addColumn({progid:"DecalControls.CheckColumn"}) // enabled
		.addColumn({progid:"DecalControls.IconColumn"})
		.addColumn({progid:"DecalControls.TextColumn", fixedwidth:notebook.getWidth()})
		.on("onControlEvent", onControlEvent);
			
		var btnQuit = core.controls.btnQuit = new SkunkSchema.PushButton({
			parent:layout, 
			name:"btnQuit", 
			text:"Quit"
		})
		.setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT")
		.on("onControlEvent", onControlEvent)
		.setWidth(50);
		
	
		view.showControls(true);
	};
	
	core.populateScriptList = function populateScriptList() { 
		core.controls.lstScripts.clear();
		var p;
		var script;
		var enabled;
		for (var i=0; i<packages.length; i++) {
			p = packages[i];

			script = loadedScripts[p.path];
			enabled = script && script.isEnabled() || false;

			core.controls.lstScripts.addRow(
				enabled, 
				0x06000000 + (p.icon || 0),
				p.name
			);
		}
		
		/*for (var i=14000; i<15000; i++) {
			core.controls.lstScripts.addRow(false, 0x06000000 + i, i);
		}*/
	};

	core.disableSripts = function disableSripts() { // Disable all running scripts.
		var p;
		var script;
		var enabled;
		for (var i=0; i<packages.length; i++) {
			p = packages[i];
			script = loadedScripts[p.path];
			enabled = script && script.isEnabled() || false;
			if (enabled) {
				script.disable();
			};
		}
	};

	core.getPackage = function getPackage(params) {
		core.debug("<getPackage>", "name: " + params.name);
		var name = params.name;

		var p;
		for (var i=0; i<packages.length; i++) {
			p = packages[i];
			core.debug(" " + i + " " + p.name);
			if (name && p.name.match(name)) {
				return p;
			}
		}
	};

	
	core.on("onInitialize", function onInitialize() {
		core.debug("<onInitialize>");
		SkunkSchema = core.SkunkSchema  = LibStub("SkunkSchema-1.0");
	});
	
	var packages;
	core.on("onEnable", function onEnable() {
		core.debug("<onEnable>");
		packages = getPackages();
		core.createGUI();

		core.addHandler(evidOnLogon,      core);
		core.addHandler(evidOnCommand,      core);

		if (skapi.plig == pligInWorld) {
			core.showGUI();
		}
	});
	
	core.on("onDisable", function onDisable() {
		core.disableSripts();
		skapi.RemoveControls(core.title);
	});
	
	core.on("onTick", function onTick() {
		var script;
		for (var path in loadedScripts) {
			if (!loadedScripts.hasOwnProperty(path)) continue;
			script = loadedScripts[path];
			if (script) {
				script.tick();
			}
		}
	});

	core.OnLogon = function OnLogon(acoChar) {
		core.debug("<OnLogon>", acoChar);
		if (skapi.plig == pligInPortal) {
			core.showGUI();
		}
	};
	//core.on("onDisable", function onDisable() {});
	
	core.OnCommand = function OnCommand(szCmd) {
		//console.info("<OnCommand>", szCmd);
		//if (szCmd && szCmd.match(/^(enable|disable) (.*)/i)) {
		//	szCmd = "sm " + szCmd;
		//}
		
		if (szCmd && szCmd.match(/^sm (\S*) (.*)/i)) {
			var command = RegExp.$1;
			var name = RegExp.$2;
			
			if (command == "enable") {
				var p = core.getPackage({name:name});
				if (p) {
					var script = loadedScripts[p.path];
					if (script && script.isEnabled()) {
						core.info(p.name + " is already enabled.");
						return;
					}
					core.info("Enabling " + p.name + "...");
					enableScript({path:p.path});
					core.info(p.name + " enabled.");
				} else {
					core.info("Could not find script: " + name);
				}
			} else if (command == "disable") {
				var p = core.getPackage({name:name});
				if (p) {
					var script = loadedScripts[p.path];
					if (!script) {
						core.info(p.name + " is not loaded.");
						return;
					} else if (!script.isEnabled()) {
						core.info(p.name + " is already disabled.");
						return;
					}
					core.info("Disabling " + p.name + "...");
					disableScript({path:p.path});
					core.info(p.name + " disabled.");
				} else {
					core.info("Could not find script: " + name);
				}
			} else {
				core.info("Unknown command: " + command);
			}
			return;
		}
	};
	
	core.showGUI = function showGUI() {
		var xml = core.controls.view.toXML();
		
		skapi.ShowControls(xml, (console.szScript.indexOf("ScriptManager.swx") >= 0));
		core.populateScriptList();
	};
	
	/**
		* getJsonFile() Get the contents of a text file that is formatted as json.
	 *
	 * @param {object} params: Collection of arguments.
	 * @param {string} params.path: File path to the file.
	 * @return {variable} Contents of the file.
	 */
	core.getJsonFile = function getJsonFile( params ) {
		if (core.existsSync(params.path)) {
			var json = core.readFileSync(params.path);
			var items;
			try {
				items = JSON.parse(json);
			} catch (e) {
				return;
			}
			return items;
		}
	};
	
	core.main = function main() {
		core.initialize();
		core.enable();
		while (core._script.enabledState == true) {
			core.emit("onTick");
			core.WaitEvent(1000, wemFullTimeout);
		}
		console.StopScript();
	};
	
	return core;
}));

if (typeof ScriptManager10 !== "undefined") {
	main = ScriptManager10.main;
}